import { DataTypes, ModelDefined } from 'sequelize';
import sequelizeConnection from '../config/database';

export type UserAttributes = {
    ID: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Address: string;
    AuthToken: string | null;
    Password: string;
    IsAdmin: boolean;
    LineID?: number;
};

type UserCreationAttributes = Omit<UserAttributes, 'ID'>;

export const User: ModelDefined<UserAttributes, UserCreationAttributes> = sequelizeConnection.define(
    'app_users',
    {
        ID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        FirstName: {
            type: DataTypes.STRING,
        },
        LastName: {
            type: DataTypes.STRING,
        },
        Email: {
            type: DataTypes.STRING,
        },
        Address: {
            type: DataTypes.STRING,
        },
        AuthToken: {
            type: DataTypes.STRING,
        },
        Password: {
            type: DataTypes.STRING,
        },
        IsAdmin: {
            type: DataTypes.BOOLEAN,
        },
        LineID: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'bus_lines',
                key: 'LineID'
            }
        }
    }
);

sequelizeConnection.sync().then(() => {
    console.log('User table created successfully!');
}).catch((error) => {
    console.error('Unable to create table : ', error);
});

export default User;