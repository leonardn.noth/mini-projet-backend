import { DataTypes, ModelDefined } from 'sequelize';
import sequelizeConnection from '../config/database';
import BusLine from "./BusLine";

export type BusRecordingAttributes = {
    ID: number;
    RecordedAtTime: string;
    ExpectedArrivalTime: string;
    ScheduledArrivalTime: string;
    LineID: number;
    UserID: number;
};

type BusRecordingCreationAttributes = Omit<BusRecordingAttributes, 'ID'>;

export const BusRecording: ModelDefined<BusRecordingAttributes, BusRecordingCreationAttributes> = sequelizeConnection.define(
    'bus_recordings',
    {
        ID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        RecordedAtTime: {
            type: DataTypes.STRING,
        },
        ExpectedArrivalTime: {
            type: DataTypes.STRING,
        },
        ScheduledArrivalTime: {
            type: DataTypes.STRING,
        },
        LineID: {
            type: DataTypes.INTEGER,
            references: {
                model: 'bus_lines',
                key: 'LineID'
            }
        },
        UserID: {
            type: DataTypes.INTEGER,
            references: {
                model: 'app_users',
                key: 'ID'
            }
        }
    }
);

// Define the association
BusRecording.belongsTo(BusLine, { foreignKey: 'LineID', targetKey: 'LineID' });
BusLine.hasMany(BusRecording, { foreignKey: 'LineID', sourceKey: 'LineID' });


sequelizeConnection.sync().then(() => {
    console.log('Bus recording table created successfully!');
}).catch((error) => {
    console.error('Unable to create table : ', error);
});

export default BusRecording;