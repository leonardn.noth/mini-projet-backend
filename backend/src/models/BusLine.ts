import { DataTypes, ModelDefined } from 'sequelize';
import sequelizeConnection from '../config/database';
import BusRecording from "./BusRecording";

export type BusLineAttributes = {
    LineID: number;
    PublishedLineName: string;
    OriginName: string;
    OriginLatitude: number;
    OriginLongitude: number;
    DestinationName: string;
    DestinationLatitude: number;
    DestinationLongitude: number;
};

type BusLineCreationAttributes = Omit<BusLineAttributes, 'LineID'>;

export const BusLine: ModelDefined<BusLineAttributes, BusLineCreationAttributes> = sequelizeConnection.define(
    'bus_lines',
    {
        LineID: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        PublishedLineName: {
            type: DataTypes.STRING,
        },
        OriginName: {
            type: DataTypes.STRING,
        },
        OriginLatitude: {
            type: DataTypes.DOUBLE,
        },
        OriginLongitude: {
            type: DataTypes.DOUBLE,
        },
        DestinationName: {
            type: DataTypes.STRING,
        },
        DestinationLatitude: {
            type: DataTypes.DOUBLE,
        },
        DestinationLongitude: {
            type: DataTypes.DOUBLE,
        },
    }
);

sequelizeConnection.sync().then(() => {
    console.log('Bus line table created successfully!');
}).catch((error) => {
    console.error('Unable to create table : ', error);
});

export default BusLine;
