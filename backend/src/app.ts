import * as dotenv from "dotenv";
dotenv.config({path:"./.env" });
import express, {Express} from 'express';
import cors from 'cors';

import {userRouter} from './middleware/user'
import {busLinesRouter} from "./middleware/busLines";
import {busRecordingsRouter} from "./middleware/busRecordings";

const app:Express = express();

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('Hello, world!');
});

app.use('/users', userRouter);
app.use('/bus_lines', busLinesRouter);
app.use('/bus_recordings', busRecordingsRouter);


export default app;
