import { Router, Request, Response } from 'express';
import BusRecording from '../models/BusRecording';
import User, {UserAttributes} from '../models/User';
import { verifyToken } from './user';
import BusLine from '../models/BusLine';

const busRecordingsRouter = Router();

busRecordingsRouter.use((req, res, next) => {
    console.log('Time: ', Date.now());
    next();
});



busRecordingsRouter.post('/list', verifyToken, async (req: Request, res: Response) => {
    try {
        const { LineID: requestedLineID, UserID } = req.body;

        // Verify the LineID for the user
        const user = await User.findOne({ where: { ID: UserID } }) as unknown as UserAttributes;
        if (!user || user.LineID !== requestedLineID) {
            return res.status(401).json({ message: 'Unauthorized: LineID does not match your assigned line.' });
        }

        // Fetch bus recordings and include the BusLine data
        const busRecordings = await BusRecording.findAll({
            where: {
                UserID: UserID,
                LineID: requestedLineID,
            },
            include: [{
                model: BusLine,
                attributes: ['PublishedLineName'],
            }],
        }) as any;

        // Transform the data to include PublishedLineName along with LineID
        const transformedData = busRecordings.map((record) => {
            return {
                ...record.get(),
                PublishedLineName: record.bus_line.PublishedLineName, // Assuming BusLine is the association alias
            };
        });

        const transformedData2 = transformedData.map((record) => {
            let temp = record;
            temp.ArrivalTime = record.ScheduledArrivalTime;
            delete temp.ScheduledArrivalTime;
            console.log(temp)
            return temp;
        });

        console.log(transformedData2);


        res.status(200).json(transformedData2);
    } catch (err) {
        console.log(err);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

busRecordingsRouter.post('/new', verifyToken, async (req: Request, res: Response) => {
    try {
        const { UserID, LineID, ArrivalTime, ExpectedArrivalTime, RecordedAt } = req.body;

        // Validate request body
        if (!UserID || !LineID || !ArrivalTime || !ExpectedArrivalTime || !RecordedAt) {
            return res.status(400).json({ message: 'Missing required fields' });
        }

        // Fetch the user to check LineID
        const user = await User.findByPk(UserID);
        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        // Check if the LineID in the request matches the user's LineID
        //@ts-ignore
        if (user.LineID !== LineID) {
            return res.status(401).json({ message: 'Unauthorized: LineID does not match user’s assigned line.' });
        }

        // Create a new BusRecording entry
        const newRecording = await BusRecording.create({
            UserID,
            LineID,
            ScheduledArrivalTime: ArrivalTime,
            ExpectedArrivalTime,
            RecordedAtTime: RecordedAt
        });

        // Fetch the PublishedLineName from BusLine
        const busLine = await BusLine.findByPk(LineID);
        //@ts-ignore
        const publishedLineName = busLine ? busLine.PublishedLineName : null;

        // Prepare response data
        const responseData = {
            ...newRecording.get(),
            PublishedLineName: publishedLineName
        };

        res.status(201).json(responseData);
    } catch (error) {
        console.error('Error in creating bus recording: ', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

export { busRecordingsRouter };
