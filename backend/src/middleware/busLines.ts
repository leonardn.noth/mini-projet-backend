import {Router} from 'express';
import BusLine, {BusLineAttributes} from "../models/BusLine";


const busLinesRouter = Router();



busLinesRouter.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})


busLinesRouter.get("/list", async (req, res) => {
    try {
        const busLines = await BusLine.findAll() as unknown as BusLineAttributes[];
        const transformedBusLines = busLines.map((busLine) => ({
            "String": `${busLine.PublishedLineName} (${busLine.OriginName} -> ${busLine.DestinationName})`,
            "LineID": busLine.LineID
        }));

        console.log(transformedBusLines);
        res.status(200).json(transformedBusLines);
    } catch (err) {
        console.log(err);
    }
});


export { busLinesRouter };
