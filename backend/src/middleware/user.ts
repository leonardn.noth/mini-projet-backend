import {User, UserAttributes} from "../models/User";
import crypto from "crypto";
import {Router} from 'express';

const jwt = require("jsonwebtoken");

const userRouter = Router();


const verifyToken = async (req, res, next) => {
    const token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send("A token is required for authentication");
    }

    try {
        req.user = jwt.verify(token, process.env.TOKEN_KEY);
        next();
    } catch (err) {
        return res.status(401).send("Invalid Token");
    }
};

const verifyAdmin = async (req, res, next) => {
    const token = req.headers["x-access-token"];

    if (!token) {
        return res.status(403).send("A token is required for authentication");
    }

    try {
        const decoded = jwt.verify(token, process.env.TOKEN_KEY);
        const user = await User.findOne({ where: { Email: decoded.Email } }) as unknown as UserAttributes;

        if (!user || !user.IsAdmin) {
            return res.status(403).send("Unauthorized. Admin access only.");
        }

        req.user = decoded;
        return next();
    } catch (err) {
        return res.status(401).send("Invalid Token");
    }
};


userRouter.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
})

userRouter.post("/login", async (req, res) => {
    try {
        // Get user input
        const {Email, Password} = req.body;

        // Validate user input
        if (!(Email && Password)) {
            res.status(400).send("All input is required");
        }
        // Validate if user exist in our database
        const user: any = await User.findOne({where: {Email}});

        if (user && (user.Password === Password)) {
            // Create token
            // save user token
            user.AuthToken = jwt.sign(
                {
                    Email: Email,
                    Address: user.Address,
                    LineID: user.LineID,
                    FirstName: user.FirstName,
                    LastName: user.LastName
                },
                process.env.TOKEN_KEY as string,
                {
                    expiresIn: "8h",
                }
            );

            await user.save();

            const userWithoutPassword = {
                ID: user.ID,
                FirstName: user.FirstName,
                LastName: user.LastName,
                Email: user.Email,
                Address: user.Address,
                AuthToken: user.AuthToken,
                LineID: user.LineID
            };

            res.status(200).json(userWithoutPassword);
        } else {
            res.status(400).send("Invalid Credentials");
        }
    } catch (err) {
        console.log(err);
    }
});
userRouter.post("/register", async (req, res) => {
    try {
        // Get user input
        const {FirstName, LastName, Email, Password, Address, LineID} = req.body;

        // Validate user input
        if (!(FirstName && LastName && Email && Password && Address && LineID)) {
            return res.status(400).send("All input is required");
        }

        // check if user already exist
        // Validate if user exist in our database
        const oldUser: any  = await User.findOne({where: {Email}});

        if (oldUser) {
            return res.status(409).send("User Already Exist. Please Login");
        }

        // Create user in our database
        const user = await User.create({
            FirstName,
            LastName,
            Email: Email.toLowerCase(), // sanitize: convert email to lowercase
            Password: Password,
            Address,
            LineID,
            AuthToken: jwt.sign(
                {Email, Address, LineID, FirstName, LastName, IsAdmin: false},
                process.env.TOKEN_KEY as string,
                {
                    expiresIn: "8h",
                }
            ),
            IsAdmin: false
        }) as unknown as UserAttributes


        const userWithoutPassword = {
            ID: user.ID,
            FirstName: user.FirstName,
            LastName: user.LastName,
            Email: user.Email,
            Address: user.Address,
            AuthToken: user.AuthToken,
            LineID: user.LineID
        };

        res.status(200).json(userWithoutPassword);
    } catch (err) {
        console.log(err);
    }
});
userRouter.post("/logout", async (req, res) => {
    const { Email } = req.body;

    if (!Email) {
        return res.status(400).json({ message: "Email is required" });
    }

    try {
        const user: any = await User.findOne({where: {Email}});

        if (!user) {
            return res.status(404).json({ message: "User not found" });
        }

        user.AuthToken = null;
        await user.save();

        return res.status(200).json({ message: "Successfully logged out" });
    } catch (error) {
        console.error('Error during logout:', error);
        return res.status(500).json({ message: "Internal Server Error" });
    }
});
userRouter.post("/check-token", async (req, res) => {
    const wrapper = (req, res):Promise<void> => {
        return new Promise((resolve, reject) => {
            const next = () => {
                resolve();
            };
            verifyToken(req, res, next);
        });
    };

    try {
        await wrapper(req, res);
        res.status(200).json({ message: "Valid Token" });
    } catch (err) {
        // Note: You don't need to handle the error here as verifyToken will respond with the relevant HTTP status code.
    }
});

userRouter.post("/update", verifyToken, async (req, res) => {
    try {
        const { FirstName, LastName, Email, Password, Address, LineID, UserID } = req.body;

        // Validate user input
        if (!(FirstName && LastName && Email && Address && LineID)) {
            return res.status(400).send("All input is required");
        }

        // Check if user exists in our database
        const userToUpdate: any = await User.findOne({ where: { ID: UserID } });

        if (!userToUpdate) {
            return res.status(404).send("User not found");
        }

        // Update user details in the database
        await User.update(
            { FirstName, LastName, Email, Password, Address, LineID },
            {
                where: { ID: UserID },
            },

        );

        const user: any = await User.findOne({ where: { ID: UserID } });

        const userWithoutPassword = {
            ID: user.ID,
            FirstName: user.FirstName,
            LastName: user.LastName,
            Email: user.Email,
            Address: user.Address,
            AuthToken: user.AuthToken,
            LineID: user.LineID
        };

        console.log(user)
        console.log(userWithoutPassword)

        res.status(200).json(userWithoutPassword);
    } catch (err) {
        console.log(err);
        return res.status(500).send("An error occurred while updating the profile");
    }
});


userRouter.get("/drivers", async (req, res) => {
    try {
        const drivers = await User.findAll() as unknown as UserAttributes[];
        res.status(200).json(drivers.map((driver) => `${driver.FirstName} ${driver.LastName} (${driver.Email})`));
    } catch (err) {
        console.log(err);
    }

});

// Testing routes
userRouter.get("/profile", verifyToken, async (req, res) => {
    res.status(200).json({message: "Congrats! You are authorized to see this secret message as you have a valid token."});
});
userRouter.get("/admin", verifyAdmin, async (req, res) => {
    res.status(200).json({message: "Congrats! You are authorized to see this secret message as you have a valid token and you are admin."});
});


export { verifyToken, userRouter };
