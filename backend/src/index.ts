import http from "http";
import app from "./app";
const server = http.createServer(app);

const port = process.env.API_PORT;

// server listening
server.listen(port, () => {
    console.log(`Server running on port ${port}`);
});